<?php
/*
Template Name: Homepage
 */
?>

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();?>

<body <?php body_class();?>>

      <div class="container">
            <section class="intro-section">
                  <div class="intro row">
                        <div class="photo"><img src="<?php the_field('intro-image'); ?>" /></div>
                        <div class="intro-content">
                              <?php the_field('Intro'); ?>
                        </div>
                  </div>
            </section>
            <section class="download">
                  <a target="_blank" href="<?php the_field('download-url'); ?>"><button><?php the_field('download-label'); ?></button></a>
            </section>
            <section class="skills">
                  <h2>
                        <span><?php the_field('skills-title'); ?></span>
                  </h2>
                  <div class="row skill-row">
                        <div class="skills-left skills-column">
                              <?php the_field('skills-left'); ?>
                        </div>
                        <div class="skills-right skills-column">
                              <?php the_field('skills-right'); ?>
                        </div>
                  </div>
                  <div class="skills-center skills-column">
                        <?php the_field('skills-center'); ?>
                  </div>
            </section>
            <section class="projects">
                  <?php while (have_posts()): the_post();?>
                  <?php the_content();?>
                  <?php endwhile; // end of the loop. ?>
            </section>
            <section class="contact">
                  <h2><span>Let's Talk</span></h2>
                  <div class="contact-content row">
                        <div class="form">
                              <?php echo do_shortcode("[ninja_form id='1']"); ?>
                        </div>
                  </div>
            </section>
      </div>
</body>

<?php get_footer();?>